package tominaga_ashiri.service;

import static tominaga_ashiri.utils.CloseableUtil.*;
import static tominaga_ashiri.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tominaga_ashiri.beans.Branch;
import tominaga_ashiri.dao.BranchDao;

public class BranchService {

	public List<Branch> select(){

		Connection connection = null;
		try {
			connection = getConnection();
			List<Branch> branches = new BranchDao().select(connection);
			commit(connection);

			return branches;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

}
