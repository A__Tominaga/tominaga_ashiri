package tominaga_ashiri.service;

import static tominaga_ashiri.utils.CloseableUtil.*;
import static tominaga_ashiri.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tominaga_ashiri.beans.Department;
import tominaga_ashiri.dao.DepartmentDao;

public class DepartmentService {

	public List<Department> select(){

		Connection connection = null;
		try {
			connection = getConnection();
			List<Department> departments = new DepartmentDao().select(connection);
			commit(connection);

			return departments;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
