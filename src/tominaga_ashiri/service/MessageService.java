package tominaga_ashiri.service;

import static tominaga_ashiri.utils.CloseableUtil.*;
import static tominaga_ashiri.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import tominaga_ashiri.beans.Message;
import tominaga_ashiri.beans.UserMessage;
import tominaga_ashiri.dao.MessageDao;
import tominaga_ashiri.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String start, String end, String category) {

		final int LIMIT_NUM = 1000;
		String startDate = null;
		String endDate = null;

		Connection connection = null;
		if (!StringUtils.isEmpty(start)) {
			startDate = start + " 00:00:00";
		} else {
			startDate = "2020-01-01 00:00:00";

		}
		if (!StringUtils.isEmpty(end)) {
			endDate = end + " 23:59:59";
		} else {
			endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}

		if (StringUtils.isBlank(category)) {
			category = null;
		}

		try {
			connection = getConnection();
			List<UserMessage> userMessages = new UserMessageDao().select(connection, LIMIT_NUM, startDate, endDate,category);
			commit(connection);

			return userMessages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
