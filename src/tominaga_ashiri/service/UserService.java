package tominaga_ashiri.service;

import static tominaga_ashiri.utils.CloseableUtil.*;
import static tominaga_ashiri.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import tominaga_ashiri.beans.User;
import tominaga_ashiri.beans.UserBranchDepartment;
import tominaga_ashiri.dao.UserBranchDepartmentDao;
import tominaga_ashiri.dao.UserDao;
import tominaga_ashiri.utils.CipherUtil;

public class UserService {

	public void insert(User user) {
		Connection connection = null;
		try {
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().insert(connection, user);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void update(User user) {
		Connection connection = null;
		try {
			if(!StringUtils.isBlank(user.getPassword())) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}
			connection = getConnection();
			new UserDao().update(connection, user);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void stop(int userId, int isStopped) {
		Connection connection = null;
		try {
			connection = getConnection();
			new UserDao().stop(connection, userId, isStopped);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public List<UserBranchDepartment> select(){

		Connection connection = null;
		try {
			connection = getConnection();
			List<UserBranchDepartment> users = new UserBranchDepartmentDao().select(connection);
			commit(connection);

			return users;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public User select(int userId){

		Connection connection = null;
		try {
			connection = getConnection();
			User user = new UserDao().select(connection, userId);
			commit(connection);

			return user;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public User select(String account, String password) {
		Connection connection = null;
		try {
			String encPassword = CipherUtil.encrypt(password);

			connection = getConnection();
			User user = new UserDao().select(connection, account, encPassword);
			commit(connection);

			return user;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public User select(String account) {

		Connection connection = null;
		try {
			connection = getConnection();
			User user = new UserDao().select(connection, account);
			commit(connection);

			return user;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}

	}
}
