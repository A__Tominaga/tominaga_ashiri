package tominaga_ashiri.service;

import static tominaga_ashiri.utils.CloseableUtil.*;
import static tominaga_ashiri.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import tominaga_ashiri.beans.Comment;
import tominaga_ashiri.beans.UserComment;
import tominaga_ashiri.dao.CommentDao;
import tominaga_ashiri.dao.UserCommentDao;

public class CommentService {

	public void insert(Comment comment) {
		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().insert(connection, comment);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public List<UserComment> select() {
		Connection connection = null;
		try {
			connection = getConnection();
			List<UserComment> userComments = new UserCommentDao().select(connection);
			commit(connection);

			return userComments;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public void delete(int commentId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().delete(connection, commentId);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
