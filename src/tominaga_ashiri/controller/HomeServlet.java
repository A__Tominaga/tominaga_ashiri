package tominaga_ashiri.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tominaga_ashiri.beans.UserComment;
import tominaga_ashiri.beans.UserMessage;
import tominaga_ashiri.service.CommentService;
import tominaga_ashiri.service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category = request.getParameter("category");
		List<UserMessage> userMessages = new MessageService().select(start, end, category);

		List<UserComment> userComments = new CommentService().select();

		request.setAttribute("start", start);
		request.setAttribute("end", end);
		request.setAttribute("category", category);
		request.setAttribute("userMessages", userMessages);
		request.setAttribute("userComments", userComments);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}
