package tominaga_ashiri.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tominaga_ashiri.beans.Comment;
import tominaga_ashiri.beans.User;
import tominaga_ashiri.service.CommentService;


@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		HttpSession session = request.getSession();
		Comment comment = new Comment();
		User user = (User)session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		comment.setText(request.getParameter("comment"));
		comment.setUserId(user.getId());
		comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));

		if(!isValid(errorMessages, comment.getText())) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		new CommentService().insert(comment);
		response.sendRedirect("./");


	}

	private boolean isValid(List<String> errorMessages, String text) {

		if(text.length() > 500) {
			errorMessages.add("コメントは500文字以内で入力してください。");
		}
		if(StringUtils.isBlank(text)) {
			errorMessages.add("コメントを入力してください");
		}
		if(errorMessages.size() != 0) {
			return false;
		}

		return true;
	}

}
