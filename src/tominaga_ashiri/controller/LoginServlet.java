package tominaga_ashiri.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tominaga_ashiri.beans.User;
import tominaga_ashiri.service.UserService;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		request.getRequestDispatcher("login.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		User user = new UserService().select(account, password);

		List<String> errorMessages = new ArrayList<String>();

		if(!isValid(errorMessages, account, password, user)) {
			request.setAttribute("account", account);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		session.removeAttribute("errorMessages");
		response.sendRedirect("./");
	}

	private boolean isValid(List<String> errorMessages, String account, String password,  User user) {

		if(StringUtils.isEmpty(account)) {
			errorMessages.add("アカウントが入力されていません。");
		}
		if(StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードが入力されていません。");
		}
		if(user == null || (user != null && user.getIsStopped() == 1)) {
			errorMessages.add("アカウント名またはパスワードが間違っています。");
		}
		if(errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}
