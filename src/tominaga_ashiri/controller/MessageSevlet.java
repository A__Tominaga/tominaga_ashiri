package tominaga_ashiri.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import tominaga_ashiri.beans.Message;
import tominaga_ashiri.beans.User;
import tominaga_ashiri.service.MessageService;

@WebServlet(urlPatterns = {"/message"})
public class MessageSevlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		request.getRequestDispatcher("message.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		Message message = new Message();
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		message.setUserId(user.getId());
		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));

		if(!isValid(errorMessages, message)) {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}


		new MessageService().insert(message);
		response.sendRedirect("./");

	}

	private boolean isValid(List<String> errorMessages, Message message) {

		if(message.getTitle().length() > 30) {
			errorMessages.add("タイトルは30文字以内で入力してください。");
		}
		if(message.getCategory().length() > 10) {
			errorMessages.add("カテゴリーは10文字以内で入力してください。");
		}
		if(message.getText().length() > 1000) {
			errorMessages.add("本文は1000字以内で入力してください。");
		}
		if(StringUtils.isBlank(message.getTitle())) {
			errorMessages.add("タイトルを入力してください。");
		}
		if(StringUtils.isBlank(message.getCategory())) {
			errorMessages.add("カテゴリーを入力してください。");
		}
		if(StringUtils.isBlank(message.getText())) {
			errorMessages.add("タイトルを入力してください。");
		}
		if(errorMessages.size() != 0) {
			return false;
		}

		return true;
	}

}
