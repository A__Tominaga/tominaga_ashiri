package tominaga_ashiri.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import tominaga_ashiri.beans.Branch;
import tominaga_ashiri.beans.Department;
import tominaga_ashiri.beans.User;
import tominaga_ashiri.service.BranchService;
import tominaga_ashiri.service.DepartmentService;
import tominaga_ashiri.service.UserService;

@WebServlet (urlPatterns = {"/signup"})
public class SignupServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException{
		List<String> errorMessages = new ArrayList<String>();

		User user = getUser(request);

		if(!isValid(errorMessages, user)) {
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();
			request.setAttribute("user", user);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);
		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException{

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setPasswordForConfirmation(request.getParameter("passwordForConfirmation"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(List<String> errorMessages, User user) {
		String account = user.getAccount();
		String password = user.getPassword();
		String passwordForConfirmation = user.getPasswordForConfirmation();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();
		User userAlreadyUsed = new UserService().select(account);

		if(StringUtils.isBlank(account)) {
			errorMessages.add("アカウントが入力されていません。");
		}
		if(StringUtils.isBlank(password)) {
			errorMessages.add("パスワードが入力されていません。");
		}
		if(StringUtils.isBlank(name)) {
			errorMessages.add("ユーザ名を入力してください。");
		}
		if(!StringUtils.isBlank(account) && account.length() > 20) {
			errorMessages.add("アカウントは20文字以下で入力してください。");
		}
		if(!StringUtils.isBlank(account) && account.length() < 6 ) {
			errorMessages.add("アカウントは6文字以上で入力してください。");
		}
		if(!StringUtils.isBlank(account) && (!account.matches("^[a-zA-Z0-9]+$"))) {
			errorMessages.add("アカウントに使用できる文字は半角英数字のみです。");
		}
		if(!StringUtils.isBlank(password) && password.length() > 20) {
			errorMessages.add("パスワードは20文字以下で入力してください。");
		}
		if(!StringUtils.isBlank(password) && password.length() < 6 ) {
			errorMessages.add("パスワードは6文字以上で入力してください。");
		}
		if(!StringUtils.isBlank(password) && (!password.matches("^[\\p{Alnum}|\\p{Punct}]+$"))) {
			errorMessages.add("パスワードに使用できる文字は半角英数字と記号のみです。");
		}
		if(branchId == 1 && !(departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが不正です。");
		}
		if(branchId != 1 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが不正です。");
		}
		if(!StringUtils.isBlank(name) && name.length() > 10 ) {
			errorMessages.add("名前は10文字以下で入力してください。");
		}
		if(!StringUtils.isBlank(password) && !password.equals(passwordForConfirmation)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません。");
		}
		if(userAlreadyUsed != null) {
			errorMessages.add("アカウントが重複しています。");
		}

		if(errorMessages.size() != 0){
			return false;
		}

		return true;
	}

}