package tominaga_ashiri.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tominaga_ashiri.beans.User;


@WebFilter(urlPatterns = {"/management", "/setting", "/signup", "/stop"})
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		HttpSession session =  httpRequest.getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		if(!(user.getBranchId() == 1 && user.getDepartmentId() == 1)) {
			errorMessages.add("権限が付与されていません。");
			session.setAttribute("errorMessages", errorMessages);
			httpResponse.sendRedirect("./");
			return;
		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	@Override
	public void destroy() {

	}
}
