package tominaga_ashiri.dao;

import static tominaga_ashiri.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tominaga_ashiri.beans.Branch;
import tominaga_ashiri.exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> select(Connection connection){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	id as id, ");
			sql.append("	name as name, ");
			sql.append("	created_date as created_date, ");
			sql.append("	updated_date as updated_date ");
			sql.append("FROM branches");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Branch> branches = toBranches(rs);

			return branches;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}

	public List<Branch> toBranches(ResultSet rs) throws SQLException{

		List<Branch> branches = new ArrayList<Branch>();
		try {
			while(rs.next()) {
				Branch branch = new Branch();
				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));
				branch.setCreatedDate(rs.getTimestamp("created_date"));
				branch.setUpdatedDate(rs.getTimestamp("updated_date"));
				branches.add(branch);
			}
			return branches;
		}finally {
			close(rs);
		}

	}
}
