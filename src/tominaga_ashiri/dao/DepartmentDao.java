package tominaga_ashiri.dao;

import static tominaga_ashiri.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tominaga_ashiri.beans.Department;
import tominaga_ashiri.exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> select(Connection connection){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	id as id, ");
			sql.append("	name as name, ");
			sql.append("	created_date as created_date, ");
			sql.append("	updated_date as updated_date ");
			sql.append("FROM departments");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<Department> departments = toDeparements(rs);

			return departments;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}

	public List<Department> toDeparements(ResultSet rs) throws SQLException{

		List<Department> departments = new ArrayList<Department>();
		try {
			while(rs.next()) {
				Department department = new Department();
				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));
				department.setCreatedDate(rs.getTimestamp("created_date"));
				department.setUpdatedDate(rs.getTimestamp("updated_date"));
				departments.add(department);
			}
			return departments;
		}finally {
			close(rs);
		}

	}
}
