package tominaga_ashiri.dao;

import static tominaga_ashiri.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import tominaga_ashiri.beans.User;
import tominaga_ashiri.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users(");
			sql.append("	account, ");
			sql.append("	password, ");
			sql.append("	name, ");
			sql.append("	branch_id, ");
			sql.append("	department_id, ");
			sql.append("	is_stopped, ");
			sql.append("	created_date, ");
			sql.append("	updated_date ");
			sql.append(") VALUES (");
			sql.append("	?, ");
			sql.append("	?, ");
			sql.append("	?, ");
			sql.append("	?, ");
			sql.append("	?, ");
			sql.append("	0, ");
			sql.append("	CURRENT_TIMESTAMP, ");
			sql.append("	CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());

			ps.executeUpdate();
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("	account = ?, ");
			sql.append("	name = ?, ");
			sql.append("	branch_id = ?, ");
			sql.append("	department_id = ?, ");
			if(!StringUtils.isEmpty(user.getPassword())) {
				sql.append("password = ?, ");
			}
			sql.append("	updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getDepartmentId());
			if(!StringUtils.isEmpty(user.getPassword())) {
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			}else {
				ps.setInt(5, user.getId());
			}

			ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void stop(Connection connection, int userId, int isStopped) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("	is_stopped = ?, ");
			sql.append("	updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			if(isStopped == 0) {
				ps.setInt(1, 1);
			}
			if(isStopped == 1) {
				ps.setInt(1, 0);
			}
			ps.setInt(2, userId);

			ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public User select(Connection connection, String account, String password) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM users ");
			sql.append("WHERE account = ? ");
			sql.append("AND password = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if(users.isEmpty()) {
				return null;
			}else{
				return users.get(0);
			}
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}

	public User select(Connection connection, int userId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM users ");
			sql.append("WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if(users.isEmpty()) {
				return null;
			}else{
				return users.get(0);
			}
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}

	public User select(Connection connection, String account) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * ");
			sql.append("FROM users ");
			sql.append("WHERE account = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUsers(rs);
			if(users.isEmpty()) {
				return null;
			}else if(2 <= users.size()) {
				throw new IllegalStateException("ユーザーが重複しています");
			}else {
				return users.get(0);
			}
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}
	private List<User> toUsers(ResultSet rs) throws SQLException{

		List<User> users = new ArrayList<User>();
		try {
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(Integer.parseInt(rs.getString("branch_id")));
				user.setDepartmentId(Integer.parseInt(rs.getString("department_id")));
				user.setIsStopped(Integer.parseInt(rs.getString("is_stopped")));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));

				users.add(user);
			}
			return users;
		}finally {
			close(rs);
		}
	}


}
