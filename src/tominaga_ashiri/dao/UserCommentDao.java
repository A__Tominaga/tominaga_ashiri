package tominaga_ashiri.dao;

import static tominaga_ashiri.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tominaga_ashiri.beans.UserComment;
import tominaga_ashiri.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	comments.text as text, ");
			sql.append("	comments.user_id as user_id, ");
			sql.append("	comments.created_date as created_date, ");
			sql.append("	comments.created_date as updated_date, ");
			sql.append("	comments.message_id as message_id, ");
			sql.append("	comments.id as comment_id, ");
			sql.append("	users.name as name ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date ASC " );

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserComment> userComments = toUserComments(rs);

			return userComments;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<UserComment> toUserComments(ResultSet rs) throws SQLException{
		List<UserComment> userComments = new ArrayList<UserComment>();
		try {
			while(rs.next()) {
				UserComment userComment = new UserComment();
				userComment.setCommentId(rs.getInt("comment_id"));
				userComment.setText(rs.getString("text"));
				userComment.setUserId(rs.getInt("user_id"));
				userComment.setName(rs.getString("name"));
				userComment.setMessageId(rs.getInt("message_id"));
				userComment.setCreatedDate(rs.getTimestamp("created_date"));
				userComment.setUpdatedDate(rs.getTimestamp("updated_date"));

				userComments.add(userComment);
			}
			return userComments;
		}finally {
			close(rs);
		}
	}



}
