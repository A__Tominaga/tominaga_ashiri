package tominaga_ashiri.dao;

import static tominaga_ashiri.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tominaga_ashiri.beans.UserMessage;
import tominaga_ashiri.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, int num, String start, String end, String category){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	messages.id as id,");
			sql.append("	messages.title as title,");
			sql.append("	messages.text as text,");
			sql.append("	messages.category as category,");
			sql.append("	messages.user_id as user_id,");
			sql.append("	messages.created_date as created_date,");
			sql.append("	messages.created_date as updated_date,");
			sql.append("	users.name as name ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date ");
			sql.append("BETWEEN ? AND ? ");
			if(category != null) {
				sql.append("AND messages.category = ? ");
			}
			sql.append("ORDER BY created_date DESC limit " + num + " ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, start);
			ps.setString(2, end);
			if(category != null) {
				ps.setString(3, category);
			}

			ResultSet rs = ps.executeQuery();

			List<UserMessage> userMessages = toUserMessages(rs);
			return userMessages;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}


	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException{

		List<UserMessage> userMessages = new ArrayList<UserMessage>();
		try {
			while(rs.next()) {
				UserMessage  userMessage = new UserMessage();
				userMessage.setMessageId(rs.getInt("id"));
				userMessage.setTitle(rs.getString("title"));
				userMessage.setText(rs.getString("text"));
				userMessage.setCategory(rs.getString("category"));
				userMessage.setUserId(rs.getInt("user_id"));
				userMessage.setCreatedDate(rs.getTimestamp("created_date"));
				userMessage.setUpdatedDate(rs.getTimestamp("updated_date"));
				userMessage.setName(rs.getString("name"));

				userMessages.add(userMessage);
			}
			return userMessages;
		}finally {
			close(rs);
		}
	}

}
