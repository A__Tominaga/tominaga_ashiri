package tominaga_ashiri.dao;

import static tominaga_ashiri.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tominaga_ashiri.beans.UserBranchDepartment;
import tominaga_ashiri.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	users.id as id, ");
			sql.append("	users.account as account, ");
			sql.append("	users.name as name, ");
			sql.append("	users.is_stopped as is_stopped, ");
			sql.append("	users.created_date as created_date, ");
			sql.append("	users.updated_date as updated_date, ");
			sql.append("	branches.id as branch_id, ");
			sql.append("	branches.name as branch, ");
			sql.append("	departments.id as department_id, ");
			sql.append("	departments.name as department ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY users.created_date ASC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userBranchDepartments = toUsers(rs);

			return userBranchDepartments;

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}

	}


	private List<UserBranchDepartment> toUsers(ResultSet rs) throws SQLException{

		List<UserBranchDepartment> userBranchDepartments = new ArrayList<UserBranchDepartment>();
		try {
			while(rs.next()) {
				UserBranchDepartment userBranchDepartment = new UserBranchDepartment();
				userBranchDepartment.setUserId(rs.getInt("id"));
				userBranchDepartment.setAccount(rs.getString("account"));
				userBranchDepartment.setName(rs.getString("name"));
				userBranchDepartment.setBranchId(rs.getInt("branch_id"));
				userBranchDepartment.setBranchName(rs.getString("branch"));
				userBranchDepartment.setDepartmentId(rs.getInt("department_id"));
				userBranchDepartment.setDepartmentName(rs.getString("department"));
				userBranchDepartment.setIsStopped(rs.getInt("is_stopped"));
				userBranchDepartment.setCreatedDate(rs.getTimestamp("created_date"));
				userBranchDepartment.setUpdatedDate(rs.getTimestamp("updated_date"));

				userBranchDepartments.add(userBranchDepartment);
			}
			return userBranchDepartments;
		}finally {
			close(rs);
		}

	}

}
