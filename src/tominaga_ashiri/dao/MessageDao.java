package tominaga_ashiri.dao;

import static tominaga_ashiri.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import tominaga_ashiri.beans.Message;
import tominaga_ashiri.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages (");
			sql.append("	title, ");
			sql.append("	text, ");
			sql.append("	category, ");
			sql.append("	user_id, ");
			sql.append("	created_date, ");
			sql.append("	updated_date ");
			sql.append(") VALUES (");
			sql.append("	?, ");
			sql.append("	?, ");
			sql.append("	?, ");
			sql.append("	?, ");
			sql.append("	CURRENT_TIMESTAMP, ");
			sql.append("	CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getTitle());
			ps.setString(2, message.getText());
			ps.setString(3, message.getCategory());
			ps.setInt(4, message.getUserId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int messageId) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();

			sql.append("DELETE ");
			sql.append("FROM messages ");
			sql.append("WHERE messages.id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, messageId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}
