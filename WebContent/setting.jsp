<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザー編集</title>
		<link href="./css/styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<header>
			<a href="./management">ユーザー管理</a>
		</header>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }"></c:out>
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<div class="main-contents">
			<article class="setting">
				<form action="setting" method="post">
					<dl>
						<dt><label for="account">アカウント</label></dt>
						<dd><input type="text" value="${ user.account }" name="account" id="account"></dd>
						<dt><label for="password">パスワード</label></dt>
						<dd><input type="text" name="password" id="password"></dd>
						<dt><label for="passwordForConfirmation">確認用パスワード</label></dt>
						<dd><input type="text" name="passwordForConfirmation" id="passwordForConfirmation"></dd>
						<dt><label for="">名前</label></dt>
						<dd><input type="text" value="${ user.name }" name="name" id="name"></dd>
						<dt><label for="branchId">支社</label></dt>
						<dd>
							<c:if test="${ user.id == loginUser.id }">
									<select name="branchId" id="branchId">
										<c:forEach items="${ branches }" var="branch">
											<c:if test="${ branch.id == user.branchId}">
												<option value="${ branch.id }" selected>
													<c:out value="${ branch.name }"/>
												</option>
											</c:if>
										</c:forEach>
									</select>
								</c:if>
								<c:if test="${ user.id != loginUser.id }">
									<select name="branchId" id="branchId" >
										<c:forEach items="${ branches }" var="branch">
											<c:if test="${ branch.id == user.branchId}">
												<option value="${ branch.id }" selected>
													<c:out value="${ branch.name }"/>
												</option>
											</c:if>
											<c:if test="${ branch.id != user.branchId}">
												<option value="${ branch.id }" >
													<c:out value="${ branch.name }"/>
												</option>
											</c:if>
										</c:forEach>
									</select>
								</c:if>
						</dd>
						<dt><label for="departmentId">部署</label><dt>
						<dd>
							<c:if test="${ user.id == loginUser.id }">
								<select name="departmentId" id="departmentId" >
									<c:forEach items="${ departments }" var="department">
										<c:if test="${ department.id == user.departmentId}">
											<option value="${ department.id }" selected>
												<c:out value="${ department.name }"/>
											</option>
										</c:if>
									</c:forEach>
								</select>
							</c:if>
							<c:if test="${  user.id != loginUser.id }">
								<select name="departmentId" id="departmentId">
									<c:forEach items="${ departments }" var="department">
										<c:if test="${ department.id == user.departmentId }">
											<option value="${ department.id }"  selected>
												<c:out value="${ department.name }"/>
											</option>
										</c:if>
										<c:if test="${ department.id != user.departmentId }">
											<option value="${ department.id }" >
												<c:out value="${ department.name }"/>
											</option>
										</c:if>
									</c:forEach>
								</select>
							</c:if>
						</dd>
					</dl>
					<label for="userId"></label>
					<input type="hidden" value="${ user.id }" name="userId" id="userId">
					<input type="submit" value="更新">
				</form>
			</article>
		</div>
		<footer>
			<div class="copyright">Copyright(c)Ashiri Tominaga</div>
		</footer>
	</body>
</html>