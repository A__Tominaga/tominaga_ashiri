<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ホーム</title>
		<link href="./css/styles.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			function deleteMessage(){
			    if(window.confirm('この投稿を本当に削除してよろしいですか？')){
			        return true;
			    }
			    return false;
			}
			function deleteComment(){
			    if(window.confirm('このコメントを本当に削除してよろしいですか？')){
			        return true;
			    }
			    return false;
			}
		</script>

	</head>
	<body>
		<header>
			<nav>
				<a href="./message">新規投稿</a>
				<c:if test="${loginUser.branchId == 1 && loginUser.departmentId == 1 }">
					<a href="./management">ユーザー管理</a>
				</c:if>
				<a href="./logout">ログアウト</a>
			</nav>
		</header>

		<div class="main-contents">
			<c:if test="${not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }"/>
						</c:forEach>
					</ul>
					<c:remove var="errorMessages" scope="session"></c:remove>
				</div>
			</c:if>
			<form action="./" method="get">
				<section>
					<span>日付</span>
					<label for="start"></label>
					<input type="date" name="start" id="start" value="${ start }">
					<span>～</span>
					<label for="end"></label>
					<input type="date" name="end" id="end" value="${ end }">
				</section>
				<section>
					<span>カテゴリ</span>
					<label for="category"></label>
					<input type="text" name="category" value="${ category }">
					<input type="submit" value="絞込み">
				</section>
			</form>
			<section class="message-area">
				<c:forEach items="${ userMessages }" var="userMessage">
					<article>
						<div class="message-info">
							<ul>
								<li class="title">
									<c:out value="タイトル : ${ userMessage.title }"></c:out>
								</li>
								<li class="name">
									<c:out value="投稿者 : ${ userMessage.name }"></c:out>
								</li>
								<li class="category">
									<c:out value="カテゴリー : ${ userMessage.category}"></c:out>
								</li>
							</ul>
						</div>
						<div class="message">
							<ul>
								<li>
									<div class="message-text">
										<c:forEach var="text" items="${ fn:split(userMessage.text, '
										') }">
			    								<c:out value="${ text }"></c:out><br>
										</c:forEach>
									</div>
								<li><p>投稿日 : <fmt:formatDate value="${ userMessage.createdDate }" type="Date" pattern="yyyy/MM/dd kk:mm:ss"></fmt:formatDate>
									</p>
								<li><c:if test="${ userMessage.userId == loginUser.id }">
										<form action="deleteMessage" method="post">
											<label for="deleteMessage"></label>
											<input type="submit" value="削除" onClick="return deleteMessage()">
											<input type="hidden" name="messageId" value="${userMessage.messageId }">
										</form>
									</c:if>
							</ul>
							<c:forEach items="${ userComments }" var="userComment">
								<c:if test="${ userComment.messageId == userMessage.messageId }">
									<div class="comment">
										<ul>
											<li>
												<c:out value="${ userComment.name }"></c:out>
											</li>
											<li>
												<c:forEach var="text" items="${ fn:split(userComment.text, '
												') }">
		    										<c:out value="${ text }"></c:out><br>
												</c:forEach>
											</li>
											<li>
												<fmt:formatDate value="${ userComment.createdDate }" type="Date" pattern="yyyy/MM/dd kk:mm:ss"></fmt:formatDate>
											</li>
											<li>
												<c:if test="${ userComment.userId == loginUser.id }">
													<form action="deleteComment" method="post">
														<label for="deleteComment"></label>
														<input type="submit" value="削除" onClick="return deleteComment()">
														<input type="hidden" name="commentId" value="${ userComment.commentId }">
													</form>
												</c:if>
											</li>
										</ul>
									</div>
								</c:if>

							</c:forEach>
						</div>
						<div class="comment-area">
							<form action="comment" method="post">
								<input type="hidden" name="messageId" value="${ userMessage.messageId}">
								<ul>
									<li><label for="comment">コメント入力欄</label>
									<li><textarea name="comment" id="comment" rows="12" cols="40" wrap="hard"></textarea>
									<li><input type="submit" value="コメント投稿">
								</ul>
							</form>
						</div>
					</article>
				</c:forEach>
			</section>
		</div>

		<footer>
			<div class="copyright">Copyright(c)Ashiri Tominaga</div>
		</footer>
	</body>
</html>