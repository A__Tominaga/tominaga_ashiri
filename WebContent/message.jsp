<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>新規投稿</title>
		<link href="./css/styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<header>
			<nav>
				<a href="./">ホーム</a>
			</nav>
		</header>
		<div class="main-contents">
			<c:remove var="errorMessages" scope="session"></c:remove>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }"/>
						</c:forEach>
					</ul>
				</div>
			</c:if>
			<article class="new-message">
				<form action="message" method="post">
					<dl>
						<dt><label for="title">件名</label>
						<dd><input name="title" id="title" value="${ message.title }">

						<dt><label for="category">カテゴリー</label>
						<dd><input name="category" id="category" value="${ message.category }">
						<dt><label for="text">投稿内容</label>
						<dd ><textarea name="text" id="text" rows="10" cols="60" wrap="hard">${ message.text }</textarea>

					</dl>
					<input type="submit" value="投稿">
				</form>
			</article>

		</div>
		<footer>
			<div class="copyright">Coptright(c)Ashiri Tominaga</div>
		</footer>

	</body>
</html>