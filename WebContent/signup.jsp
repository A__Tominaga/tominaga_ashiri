<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザ登録</title>
		<link href="./css/styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<header>
			<nav>
				<a href="./management">ユーザー管理</a>
			</nav>
		</header>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }"></c:out>
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<section class="signupForm">
			<article class="signup">
				<form action="signup" method="post">
					<dl>
						<dt><label for="account">アカウント</label>
						<dd><input name="account" id="account" value="${ user.account }">

						<dt><label for="password">パスワード</label>
						<dd><input name="password" id="password" >

						<dt><label for="passwordForConfirmation">確認用パスワード</label>
						<dd><input name="passwordForConfirmation" id="passwordForConfirmation">

						<dt><label for="name">名前</label>
						<dd><input name="name" id="name" value="${ user.name }">

						<dt><label for="branchId">支社</label>
						<dd><select name="branchId" id="branchId">
								<c:forEach items="${ branches }" var="branch">
									<c:if test="${ branch.id == user.branchId}">
										<option value="${ branch.id }" selected>
											<c:out value="${ branch.name }"/>
										</option>
									</c:if>
									<c:if test="${ branch.id != user.branchId}">
										<option value="${ branch.id }" >
											<c:out value="${ branch.name }"/>
										</option>
									</c:if>
								</c:forEach>
							</select>
						<dt><label for="departmentId">部署</label>
						<dd><select name="departmentId" id="departmentId">
								<c:forEach items="${ departments }" var="department">
										<c:if test="${ department.id == user.departmentId }">
											<option value="${ department.id }"  selected>
												<c:out value="${ department.name }"/>
											</option>
										</c:if>
										<c:if test="${ department.id != user.departmentId }">
											<option value="${ department.id }" >
												<c:out value="${ department.name }"/>
											</option>
										</c:if>
									</c:forEach>
								</select>
					</dl>
						<input type="submit" value="登録">
				</form>
			</article>
		</section>
	<footer>
		<div class="copyright">Copyright(c)Ashiri Tominaga</div>
	</footer>
	</body>
</html>