<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン</title>
		<link href="./css/styles.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }"/>
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"></c:remove>
		</c:if>
		<div class="main-contents">
			<article>
				<form action="login" method="post">
					<dl>
						<dt><label for="account">アカウント</label>
						<dd><input name="account" id="account" value="${ account }">

						<dt><label for="password">パスワード</label>
						<dd><input name="password" id="password">

					</dl>
						<input type="submit" value="ログイン">
				</form>
			</article>
		</div>
		<footer>
			<div class="copyright">Coptright(c)Ashiri Tominaga</div>
		</footer>
	</body>
</html>