<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザ一覧</title>
		<link href="./css/styles.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			function stopOrActiveUser(string){
			    if(window.confirm('このをユーザを' + string +'してよろしいですか？')){
			        return true;
			    }
			    return false;
			}
		</script>
	</head>
	<body>
		<header>
			<nav>
				<a href="./">ホーム</a>
				<a href="./signup">ユーザー新規登録</a>
			</nav>
		</header>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }"></c:out>
					</c:forEach>
				</ul>
				<c:remove var="errorMessages" scope="session"></c:remove>
			</div>
		</c:if>
		<section class="main-contents">
			<c:forEach items="${ userBranchDepartments }" var="userBranchDepartment">
				<article class="user">
					<dl>
						<dt>アカウント：
						<dd><c:out value="${ userBranchDepartment.account }"/>
						<dt>名前：
						<dd><c:out value="${ userBranchDepartment.name }"/>
						<dt>支社：
						<dd><c:out value="${ userBranchDepartment.branchName }"/>
						<dt>部署：
						<dd><c:out value="${ userBranchDepartment.departmentName }"/>

					</dl>
					<form action="stop" method="post">
						<input type="hidden" value="${ userBranchDepartment.userId }" name="userId">
						<input type="hidden" value="${ userBranchDepartment.isStopped }" name="isStopped">
							<c:if test="${ userBranchDepartment.isStopped == 0 }">
								<c:if test="${ loginUser.id == userBranchDepartment.userId }">
									<input type="submit" value="停止" disabled>
								</c:if>
								<c:if test="${ loginUser.id != userBranchDepartment.userId }">
									<input type="submit" value="停止"  onClick="return stopOrActiveUser('停止')">
								</c:if>
							</c:if>
							<c:if test="${ userBranchDepartment.isStopped == 1 }">
								<c:if test="${ loginUser.id == userBranchDepartment.userId }">
									<input type="submit" value="復活" disabled>
								</c:if>
								<c:if test="${ loginUser.id != userBranchDepartment.userId }">
									<input type="submit" value="復活" onClick="return stopOrActiveUser('復活')">
								</c:if>
							</c:if>
					</form>
					<form action="setting" method="get">
						<label for="userId"></label>
						<input type="hidden" value="${ userBranchDepartment.userId }" name="userId">
						<input type="submit" value="編集">
					</form>
				</article>
			</c:forEach>
		</section>
		<footer>
			<div class="copyright">Copyright(c)Ashiri Tominaga</div>
		</footer>

	</body>
</html>